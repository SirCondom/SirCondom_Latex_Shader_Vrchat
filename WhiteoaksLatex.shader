// Made with Amplify Shader Editor v1.9.2.2
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "SirCondoms/WhiteoaksLatex/Builtin"
{
	Properties
	{
		[Header(Translucency)]
		_Translucency("Strength", Range( 0 , 50)) = 1
		_TransNormalDistortion("Normal Distortion", Range( 0 , 1)) = 0.1
		_TransScattering("Scaterring Falloff", Range( 1 , 50)) = 2
		_TransDirect("Direct", Range( 0 , 1)) = 1
		_TransAmbient("Ambient", Range( 0 , 1)) = 0.2
		_TransShadow("Shadow", Range( 0 , 1)) = 0.9
		_EdgeLength ( "Edge length", Range( 2, 50 ) ) = 15
		_Matcap("Matcap", 2D) = "white" {}
		_Albedo("Albedo", 2D) = "white" {}
		_EmmisionColor("Emmision Color", Color) = (0,0,0,0)
		_Emmison("Emmison", Float) = 0
		[Toggle]_audiolink("audio link", Float) = 0
		_Int0("Int 0", Int) = 0
		_opac("opac", Float) = 0.999
		_tint("tint", Color) = (0,0,0,0)
		_CubeMap("CubeMap", CUBE) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
		[Header(Forward Rendering Options)]
		[ToggleOff] _GlossyReflections("Reflections", Float) = 1.0
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Tessellation.cginc"
		#include "Lighting.cginc"
		#pragma target 5.0
		#pragma shader_feature _GLOSSYREFLECTIONS_OFF
		#include "Packages/com.llealloo.audiolink/Runtime/Shaders/AudioLink.cginc"
		#undef TRANSFORM_TEX
		#define TRANSFORM_TEX(tex,name) float4(tex.xy * name##_ST.xy + name##_ST.zw, tex.z, tex.w)
		struct Input
		{
			float3 uv_texcoord;
			float3 worldNormal;
		};

		struct SurfaceOutputStandardCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			half3 Translucency;
		};

		uniform float4 _tint;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float _audiolink;
		uniform sampler2D _Matcap;
		uniform samplerCUBE _CubeMap;
		uniform float4 _CubeMap_ST;
		uniform float4 _EmmisionColor;
		uniform float _Emmison;
		uniform int _Int0;
		uniform half _Translucency;
		uniform half _TransNormalDistortion;
		uniform half _TransScattering;
		uniform half _TransDirect;
		uniform half _TransAmbient;
		uniform half _TransShadow;
		uniform float _opac;
		uniform float _EdgeLength;


		inline float AudioLinkLerpMultiline1_g7( float Sample )
		{
			return AudioLinkLerpMultiline( ALPASS_WAVEFORM + uint2( Sample * 1024, 0 ) ).rrrr;;
		}


		inline float AudioLinkLerp2_g8( float Sample )
		{
			return AudioLinkLerp( ALPASS_AUTOCORRELATOR + float2( Sample * 128., 0 ) ).r;;
		}


		inline float AudioLinkLerp2_g10( float Sample )
		{
			return AudioLinkLerp( ALPASS_AUTOCORRELATOR + float2( Sample * 128., 0 ) ).g;;
		}


		inline float4 AudioLinkLerp1_g3( float Position )
		{
			return AudioLinkLerp( ALPASS_CCSTRIP + float2( Position * 128., 0 ) ).rgba;;
		}


		inline float AudioLinkLerp3_g6( int Band, float FilteredAmount )
		{
			return AudioLinkLerp( ALPASS_FILTEREDAUDIOLINK + float2( FilteredAmount, Band ) ).r;
		}


		inline float AudioLinkData3_g5( int Band, int Delay )
		{
			return AudioLinkData( ALPASS_AUDIOLINK + uint2( Delay, Band ) ).rrrr;
		}


		inline float4 AudioLinkLerp1_g2( float Position )
		{
			return AudioLinkLerp( ALPASS_CCSTRIP + float2( Position * 128., 0 ) ).rgba;;
		}


		float4 tessFunction( appdata_full v0, appdata_full v1, appdata_full v2 )
		{
			return UnityEdgeLengthBasedTess (v0.vertex, v1.vertex, v2.vertex, _EdgeLength);
		}

		void vertexDataFunc( inout appdata_full v )
		{
		}

		inline half4 LightingStandardCustom(SurfaceOutputStandardCustom s, half3 viewDir, UnityGI gi )
		{
			#if !defined(DIRECTIONAL)
			float3 lightAtten = gi.light.color;
			#else
			float3 lightAtten = lerp( _LightColor0.rgb, gi.light.color, _TransShadow );
			#endif
			half3 lightDir = gi.light.dir + s.Normal * _TransNormalDistortion;
			half transVdotL = pow( saturate( dot( viewDir, -lightDir ) ), _TransScattering );
			half3 translucency = lightAtten * (transVdotL * _TransDirect + gi.indirect.diffuse * _TransAmbient) * s.Translucency;
			half4 c = half4( s.Albedo * translucency * _Translucency, 0 );

			SurfaceOutputStandard r;
			r.Albedo = s.Albedo;
			r.Normal = s.Normal;
			r.Emission = s.Emission;
			r.Metallic = s.Metallic;
			r.Smoothness = s.Smoothness;
			r.Occlusion = s.Occlusion;
			r.Alpha = s.Alpha;
			return LightingStandard (r, viewDir, gi) + c;
		}

		inline void LightingStandardCustom_GI(SurfaceOutputStandardCustom s, UnityGIInput data, inout UnityGI gi )
		{
			#if defined(UNITY_PASS_DEFERRED) && UNITY_ENABLE_REFLECTION_BUFFERS
				gi = UnityGlobalIllumination(data, s.Occlusion, s.Normal);
			#else
				UNITY_GLOSSY_ENV_FROM_SURFACE( g, s, data );
				gi = UnityGlobalIllumination( data, s.Occlusion, s.Normal, g );
			#endif
		}

		void surf( Input i , inout SurfaceOutputStandardCustom o )
		{
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 Albedo30 = ( _tint * tex2D( _Albedo, uv_Albedo ) );
			float3 ase_worldNormal = i.worldNormal;
			float4 NormalMap114 = tex2D( _Matcap, ( ( mul( UNITY_MATRIX_V, float4( ase_worldNormal , 0.0 ) ).xyz * 0.5 ) + 0.5 ).xy );
			float3 uv_CubeMap3 = i.uv_texcoord;
			uv_CubeMap3.xy = i.uv_texcoord.xy * _CubeMap_ST.xy + _CubeMap_ST.zw;
			float4 CubeMap144 = texCUBE( _CubeMap, uv_CubeMap3 );
			float Glossy89 = 0.47;
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float4 emmisons101 = reflect( ( _EmmisionColor * ase_lightColor * float4( ase_lightColor.rgb , 0.0 ) * _Emmison ) , float4( 0,0,0,0 ) );
			float4 temp_output_188_0 = ( ( ( Albedo30 + NormalMap114 + CubeMap144 ) + Glossy89 ) + emmisons101 );
			float Sample1_g7 = i.uv_texcoord.xy.x;
			float localAudioLinkLerpMultiline1_g7 = AudioLinkLerpMultiline1_g7( Sample1_g7 );
			float temp_output_150_0 = localAudioLinkLerpMultiline1_g7;
			float Sample2_g8 = saturate( temp_output_150_0 );
			float localAudioLinkLerp2_g8 = AudioLinkLerp2_g8( Sample2_g8 );
			float temp_output_174_0 = localAudioLinkLerp2_g8;
			float temp_output_151_0 = ( saturate( ( 0.5 - abs( ( temp_output_174_0 - temp_output_174_0 ) ) ) ) / temp_output_150_0 );
			float Sample2_g10 = saturate( temp_output_151_0 );
			float localAudioLinkLerp2_g10 = AudioLinkLerp2_g10( Sample2_g10 );
			float Position1_g3 = temp_output_151_0;
			float4 localAudioLinkLerp1_g3 = AudioLinkLerp1_g3( Position1_g3 );
			float temp_output_164_0 = ( (localAudioLinkLerp1_g3.x + (( temp_output_150_0 * localAudioLinkLerp2_g10 ) - 0.0) * (1.0 - localAudioLinkLerp1_g3.x) / (1.0 - 0.0)) % 1.0 );
			int Band3_g5 = (int)temp_output_164_0;
			int Band3_g6 = (int)temp_output_164_0;
			float FilteredAmount3_g6 = ( temp_output_164_0 * 15.0 );
			float localAudioLinkLerp3_g6 = AudioLinkLerp3_g6( Band3_g6 , FilteredAmount3_g6 );
			int Delay3_g5 = (int)localAudioLinkLerp3_g6;
			float localAudioLinkData3_g5 = AudioLinkData3_g5( Band3_g5 , Delay3_g5 );
			float Position1_g2 = localAudioLinkData3_g5;
			float4 localAudioLinkLerp1_g2 = AudioLinkLerp1_g2( Position1_g2 );
			float4 temp_output_187_0 = ( (( _audiolink )?( ( localAudioLinkLerp1_g2 * _Int0 ) ):( temp_output_188_0 )) * temp_output_188_0 );
			float4 temp_output_198_0 = ( Albedo30 * temp_output_187_0 );
			o.Albedo = temp_output_198_0.rgb;
			float4 temp_output_201_0 = ( temp_output_198_0 * temp_output_187_0 );
			o.Emission = temp_output_201_0.rgb;
			float temp_output_149_0 = 0.98;
			o.Metallic = temp_output_149_0;
			o.Smoothness = temp_output_149_0;
			o.Occlusion = temp_output_201_0.r;
			o.Translucency = temp_output_201_0.rgb;
			o.Alpha = _opac;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustom alpha:fade keepalpha fullforwardshadows exclude_path:deferred vertex:vertexDataFunc tessellate:tessFunction 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 5.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float3 worldNormal : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldNormal = worldNormal;
				o.customPack1.xyz = customInputData.uv_texcoord;
				o.customPack1.xyz = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xyz;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldNormal = IN.worldNormal;
				SurfaceOutputStandardCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandardCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=19202
Node;AmplifyShaderEditor.CommentaryNode;127;-3895.967,-1005.428;Inherit;False;1600.806;354.1249;Comment;8;120;121;122;123;124;125;126;114;MatCap;1,1,1,1;0;0
Node;AmplifyShaderEditor.ViewMatrixNode;120;-3765.779,-955.4279;Inherit;False;0;1;FLOAT4x4;0
Node;AmplifyShaderEditor.WorldNormalVector;121;-3845.967,-861.2941;Inherit;False;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;122;-3618.053,-797.2672;Float;False;Constant;_Float0;Float 0;-1;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;123;-3607.236,-925.642;Inherit;False;2;2;0;FLOAT4x4;0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;31;-1690.411,-1173.411;Inherit;False;831.0199;485.52;Comment;4;28;27;29;30;Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;124;-3439.983,-889.1962;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;125;-3264.674,-839.1193;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;28;-1618.127,-1123.411;Inherit;False;Property;_tint;tint;21;0;Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;27;-1640.412,-917.8909;Inherit;True;Property;_Albedo;Albedo;13;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;117;-1732.174,-590.3155;Inherit;False;1020.458;484.4478;Emmisions;6;104;105;96;97;101;116;Emmisions;1,1,1,1;0;0
Node;AmplifyShaderEditor.LightColorNode;104;-1651.308,-349.2269;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SamplerNode;143;-3020.474,-407.6259;Inherit;True;Property;_CubeMap;CubeMap;22;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;LockedToCube;False;Object;-1;Auto;Cube;8;0;SAMPLERCUBE;;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;126;-3069.229,-881.303;Inherit;True;Property;_Matcap;Matcap;12;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;0,0;False;1;FLOAT2;1,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;105;-1682.174,-221.8677;Inherit;False;Property;_Emmison;Emmison;15;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;96;-1650.548,-540.3155;Float;False;Property;_EmmisionColor;Emmision Color;14;0;Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;-1297.714,-1013.58;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;30;-1083.392,-991.3124;Float;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;97;-1360.252,-431.4021;Inherit;False;4;4;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;144;-2649.632,-334.621;Inherit;False;CubeMap;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;114;-2519.16,-830.1543;Float;False;NormalMap;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ReflectOpNode;116;-1171.449,-423.2156;Inherit;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;18;-634.0023,21.43886;Inherit;False;30;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;101;-935.7159,-421.4065;Float;False;emmisons;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;128;-369.0088,72.29759;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;146;-199.555,86.92418;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;102;-236.0736,240.2233;Inherit;False;101;emmisons;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;11;-1795.02,60.64963;Inherit;False;879.6398;352.4872;Comment;5;2;3;1;8;25;Normal Light;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;12;-1707.298,856.4357;Inherit;False;859.9405;424.0627;Comment;5;5;9;4;7;26;Normal View;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldNormalVector;1;-1588.553,112.786;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;7;-1448.219,1113.863;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DotProductOpNode;2;-1353.973,193.9188;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;8;-1139.38,131.9779;Float;False;LightDir;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;5;-1178.846,1099.271;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightPos;3;-1610.427,284.5461;Inherit;False;0;3;FLOAT4;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.GetLocalVarNode;26;-1674.648,1014.578;Inherit;False;-1;;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;25;-1774.344,221.4765;Inherit;False;-1;;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.WorldNormalVector;4;-1479.977,925.6635;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;115;-604.9606,91.75603;Inherit;False;114;NormalMap;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;145;-603.0349,157.2045;Inherit;False;144;CubeMap;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;89;-2468.417,62.32442;Float;False;Glossy;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;169;-1549.758,563.7089;Inherit;True;Property;_Texture0;Texture 0;20;0;Create;True;0;0;0;False;0;False;None;None;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.RegisterLocalVarNode;9;-1071.357,951.7518;Float;False;NormalDir;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;170;463.6488,1150.067;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FunctionNode;159;1043.336,1015.253;Inherit;True;ColorChordStrip;-1;;2;cfa8e3a605f54d2409f0ae5a9706c295;0;1;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.IntNode;171;1407.074,1128.73;Inherit;False;Property;_Int0;Int 0;17;0;Create;True;0;0;0;False;0;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.RangedFloatNode;173;127.9099,1068.282;Inherit;False;Property;_Float1;Float 1;18;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;176;794.6704,967.9576;Inherit;False;ColorChordStrip;-1;;3;cfa8e3a605f54d2409f0ae5a9706c295;0;1;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;156;-261.5095,760.9045;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;167;1069.297,605.5292;Inherit;False;4BandAmplitude;-1;;5;f5073bb9076c4e24481a28578c80bed5;0;2;2;INT;0;False;4;INT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;165;1059.748,788.1508;Inherit;False;4BandAmplitudeFiltered;-1;;6;3e18e71c60559ad419be81278157ae18;0;2;2;INT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;150;-129.1454,812.8077;Inherit;False;Waveform;-1;;7;86000a57e77967c4ea51f70716038ec2;0;1;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;174;31.25064,678.8745;Inherit;False;AutoCorrelator;-1;;8;c08072cb66b844942884d88404654c86;0;1;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;164;845.0415,658.3335;Inherit;True;BandPulse;-1;;9;c478702160369ce4480fa2fb6d734ffa;0;3;1;FLOAT;2.51;False;2;FLOAT;0.89;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;177;503.433,764.4244;Inherit;False;AutoCorrelatorUncorrelated;-1;;10;7fdb22cc62063814cb854a23c9992c11;0;1;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;151;352.7715,828.674;Inherit;True;DrawLine;-1;;11;b931a6c4da53ab6489d06086e5e19048;0;4;5;FLOAT;2.26;False;1;FLOAT;0;False;2;FLOAT;0.5;False;3;FLOAT;5.9;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;160;421.0959,496.1202;Inherit;True;2;2;0;FLOAT4;0,0,0,0;False;1;INT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;190;1274.104,-307.9409;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;155;450.4483,-187.9635;Inherit;True;Property;_audiolink;audio link;16;0;Create;True;0;0;0;False;0;False;0;True;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;107;-505.8479,304.0314;Inherit;False;89;Glossy;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;196;1435.201,188.883;Inherit;False;89;Glossy;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;188;470.1346,126.8732;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;198;2094.741,-319.308;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;193;1632.36,-334.9903;Inherit;False;30;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;87;2980.16,-359.1888;Float;False;True;-1;7;ASEMaterialInspector;0;0;Standard;SirCondoms/WhiteoaksLatex/Builtin;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;True;False;Back;0;False;;0;False;;False;0;False;;0;False;;False;7;Transparent;0.5;True;True;0;False;Transparent;;Transparent;ForwardOnly;12;all;True;True;True;True;0;False;;False;0;False;;255;False;;255;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;True;2;15;10;25;False;0.5;True;2;5;False;;10;False;;0;0;False;;0;False;;0;False;;0;False;;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;-1;0;14;7;0;False;0;0;False;;-1;0;False;;0;0;0;False;0.1;False;;0;False;;False;17;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;16;FLOAT4;0,0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;201;2440.797,-180.2147;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;149;2604.826,-95.48613;Inherit;False;Constant;_Metaaaaal;Metaaaaal;21;0;Create;True;0;0;0;False;0;False;0.98;1.31;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;187;1694.145,-143.8262;Inherit;True;2;2;0;FLOAT4;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;47;-2910.969,104.2628;Inherit;False;Constant;_Glossy;Glossy;19;0;Create;True;0;0;0;False;0;False;0.47;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;182;2553.414,110.9748;Inherit;False;Property;_opac;opac;19;0;Create;True;0;0;0;False;0;False;0.999;0.999;0;0;0;1;FLOAT;0
WireConnection;123;0;120;0
WireConnection;123;1;121;0
WireConnection;124;0;123;0
WireConnection;124;1;122;0
WireConnection;125;0;124;0
WireConnection;125;1;122;0
WireConnection;126;1;125;0
WireConnection;29;0;28;0
WireConnection;29;1;27;0
WireConnection;30;0;29;0
WireConnection;97;0;96;0
WireConnection;97;1;104;0
WireConnection;97;2;104;1
WireConnection;97;3;105;0
WireConnection;144;0;143;0
WireConnection;114;0;126;0
WireConnection;116;0;97;0
WireConnection;101;0;116;0
WireConnection;128;0;18;0
WireConnection;128;1;115;0
WireConnection;128;2;145;0
WireConnection;146;0;128;0
WireConnection;146;1;107;0
WireConnection;1;0;25;0
WireConnection;2;0;1;0
WireConnection;2;1;3;1
WireConnection;8;0;2;0
WireConnection;5;0;4;0
WireConnection;5;1;7;0
WireConnection;4;0;26;0
WireConnection;89;0;47;0
WireConnection;9;0;5;0
WireConnection;159;2;167;0
WireConnection;176;2;151;0
WireConnection;167;2;164;0
WireConnection;167;4;165;0
WireConnection;165;2;164;0
WireConnection;165;4;164;0
WireConnection;150;2;156;0
WireConnection;174;1;150;0
WireConnection;164;1;150;0
WireConnection;164;2;177;0
WireConnection;164;3;176;0
WireConnection;177;1;151;0
WireConnection;151;5;174;0
WireConnection;151;1;174;0
WireConnection;151;3;150;0
WireConnection;160;0;159;0
WireConnection;160;1;171;0
WireConnection;155;0;188;0
WireConnection;155;1;160;0
WireConnection;188;0;146;0
WireConnection;188;1;102;0
WireConnection;198;0;193;0
WireConnection;198;1;187;0
WireConnection;87;0;198;0
WireConnection;87;2;201;0
WireConnection;87;3;149;0
WireConnection;87;4;149;0
WireConnection;87;5;201;0
WireConnection;87;7;201;0
WireConnection;87;9;182;0
WireConnection;201;0;198;0
WireConnection;201;1;187;0
WireConnection;187;0;155;0
WireConnection;187;1;188;0
ASEEND*/
//CHKSM=2AA0FE4BEA1FC963E396368DB29986F417FEA4FB